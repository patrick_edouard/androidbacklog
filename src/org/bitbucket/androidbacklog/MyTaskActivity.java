package org.bitbucket.androidbacklog;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.LabeledIntent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyTaskActivity extends Activity {

	NoSQLEngine myEngine;
	boolean estvide;

	/*private void remplirListe() {
	    	listString.clear();

	    	for (int i=0; i<tabdeStrings.length; i++) {
	    		listString.add(tabdeStrings[i]);
	    	}

	}*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		setContentView(R.layout.activity_mytask);
		myEngine = NoSQLEngine.getInstance();
		//Log.i("jeudi",myEngine.getAgencyName());

		final ListView listTask = (ListView)findViewById(R.id.listTask);
		final ArrayList<BacklogItem> listItem = myEngine.getBacklogItems();
		
		if(listItem == null){
			Log.i("crash", "Aparement, le projet n'existe pas. L'application va crasher");
		}
		
		final List<String> listItemString = new ArrayList<String>();
		if (listItem.size() == 0){
			listItemString.add("No task");
			estvide = true;
		}
		else {
			for (BacklogItem myItem : listItem){
				//Log.i(LoginActivity.class.getName(),"new task :"+myItem.toString());
				//Log.i("TASK_LIST","task :"+ myItem.toString());
				listItemString.add(myItem.name + " - \""+myItem.commentary+"\"");
			}
			estvide = false;
		}
		ArrayAdapter<String> adapter = 
				new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItemString);
		listTask.setAdapter(adapter);

		final Button addTask = (Button) findViewById(R.id.addNewTask);
		addTask.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final AlertDialog.Builder ad = new AlertDialog.Builder(MyTaskActivity.this);
				final LinearLayout ll = new LinearLayout(MyTaskActivity.this);
				ll.setOrientation(LinearLayout.VERTICAL);

				final TextView textName = new TextView(MyTaskActivity.this);
				textName.setText("Name");
				final EditText inputName = new EditText(MyTaskActivity.this);
				inputName.setHint("Name");

				final TextView textComment = new TextView(MyTaskActivity.this);
				textComment.setText("Commentary");
				final EditText inputComment = new EditText(MyTaskActivity.this);
				inputComment.setHint("Commentary");

				final TextView textPriority = new TextView(MyTaskActivity.this);
				textPriority.setText("Priority");
				final EditText inputPriorty = new EditText(MyTaskActivity.this);
				inputPriorty.setHint("Priority");

				final TextView textEstimation = new TextView(MyTaskActivity.this);
				textEstimation.setText("Estimation");
				final EditText inputEstimation = new EditText(MyTaskActivity.this);
				inputEstimation.setHint("Estimation");

				ad.setTitle("Create a new task");
				//ll.addView(textName);
				ll.addView(inputName);
				//ll.addView(textComment);
				ll.addView(inputComment);
				//ll.addView(textPriority);
				ll.addView(inputPriorty);
				//ll.addView(textEstimation);
				ll.addView(inputEstimation);
				ad.setView(ll);
				ad.setPositiveButton("Create",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						//MyProjectActivity.this.finish();
						//String value = input.getText().toString().trim();
						//Toast.makeText(getApplicationContext(), value+" has been created !", Toast.LENGTH_SHORT).show();
						//ArrayAdapter<String> adapter;
						//adapter = new ArrayAdapter<String>(MyTaskActivity.this, android.R.layout.simple_list_item_1, listItemString);
						//adapter.remove((String) listProject.getItemAtPosition(adapter.getCount()));
						//listTask.setAdapter(adapter);
						
						if(listItem.size() == 0 ){
							if(listItemString.size() == 1 )
								listItemString.remove(0);
						}
						
						BacklogItem newItem = new BacklogItem(
								inputComment.getText().toString(),
								inputEstimation.getText().toString(),
								0,
								Integer.parseInt(inputName.getText().toString()),
								Integer.parseInt(inputPriorty.getText().toString()));								
						String value = newItem.name + " - \""+newItem.commentary+"\"";

						listItem.add(newItem);
						listItemString.add(value);

						ArrayAdapter<String> adapter;
						adapter = new ArrayAdapter<String>(MyTaskActivity.this, android.R.layout.simple_list_item_1, listItemString);
						listTask.setAdapter(adapter);

						myEngine.sendBacklogItem(newItem);

						Toast.makeText(getApplicationContext(), value + " has been created", Toast.LENGTH_SHORT).show();
						dialog.cancel();
					}
				});
				ad.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});
				ad.show();
			}
		});		

		listTask.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id)
			{
				final int positionTask = position;
				
				final AlertDialog.Builder ad = new AlertDialog.Builder(MyTaskActivity.this);
				final LinearLayout ll = new LinearLayout(MyTaskActivity.this);
				for (BacklogItem b : listItem)
					Log.i("backlog",b.toString());
				//Log.i("backlog","position: "+ position);
				final BacklogItem item = listItem.get(positionTask);
				//Log.i("backlog", "my Item : "+item.toString());
				//Log.i("backlog","setName : "+item.name);
				ll.setOrientation(LinearLayout.VERTICAL);
				final TextView textName = new TextView(MyTaskActivity.this);
				textName.setText("Name");
				final EditText inputName = new EditText(MyTaskActivity.this);
				inputName.setText(item.name+"");
				final TextView textComment = new TextView(MyTaskActivity.this);
				textComment.setText("Commentary");
				final EditText inputComment = new EditText(MyTaskActivity.this);
				inputComment.setText(item.commentary);
				final TextView textPriority = new TextView(MyTaskActivity.this);
				textPriority.setText("Priority");
				final EditText inputPriorty = new EditText(MyTaskActivity.this);
				inputPriorty.setText(item.priority+"");
				final TextView textEstimation = new TextView(MyTaskActivity.this);
				textEstimation.setText("Estimation");
				final EditText inputEstimation = new EditText(MyTaskActivity.this);
				inputEstimation.setText(item.estimation+"");

				ad.setTitle("Task : "+listTask.getItemAtPosition(positionTask));
				//ll.addView(textName);
				ll.addView(inputName);
				//ll.addView(textComment);
				ll.addView(inputComment);
				//ll.addView(textPriority);
				ll.addView(inputPriorty);
				//ll.addView(textEstimation);
				ll.addView(inputEstimation);
				ad.setView(ll);
				ad.setPositiveButton("Modify",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						//MyProjectActivity.this.finish();
						//String value = input.getText().toString().trim();
						//Toast.makeText(getApplicationContext(), value+" has been created !", Toast.LENGTH_SHORT).show();
						//ArrayAdapter<String> adapter;
						//adapter = new ArrayAdapter<String>(MyTaskActivity.this, android.R.layout.simple_list_item_1, listItemString);
						//adapter.remove((String) listProject.getItemAtPosition(adapter.getCount()));
						//listTask.setAdapter(adapter);
						String value = (String) listTask.getItemAtPosition(positionTask);
						
						//myEngine.deleteTask(item.id);

						item.name = Integer.parseInt(inputName.getText().toString());
						item.commentary = inputComment.getText().toString();
						item.priority = Integer.parseInt(inputPriorty.getText().toString());
						item.estimation = inputEstimation.getText().toString();
						listItem.set(positionTask, item);

						listItemString.set(positionTask, item.name + " - \""+item.commentary+"\"");

						ArrayAdapter<String> adapter;
						adapter = new ArrayAdapter<String>(MyTaskActivity.this, android.R.layout.simple_list_item_1, listItemString);
						listTask.setAdapter(adapter);

						myEngine.modifyTask(item);
						
						/*
						listItem.removeAll(listItem);
						myEngine.sendBacklogItem(item);
						myEngine.loadBackLogItem();
						
						
						listItem.addAll(myEngine.getBacklogItems());
						for(BacklogItem item : listItem)
							listItemString.add(item.name+" - \""+ item.commentary+"\"");
						
						adapter = new ArrayAdapter<String>(MyTaskActivity.this, android.R.layout.simple_list_item_1, listItemString);
						listTask.setAdapter(adapter);

						*/

						Toast.makeText(getApplicationContext(), value + " has been modified", Toast.LENGTH_SHORT).show();
						dialog.cancel();
					}
				});
				ad.setNegativeButton("Delete",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						
						String value = (String) listTask.getItemAtPosition(position);
						Log.i("jeudi","item id : "+item.id);
						myEngine.deleteTask(item.id);
						listItemString.remove(position);
						listItem.remove(position);
						ArrayAdapter<String> adapter;
						adapter = new ArrayAdapter<String>(MyTaskActivity.this, android.R.layout.simple_list_item_1, listItemString);
						listTask.setAdapter(adapter);

						Toast.makeText(getApplicationContext(), value + " has been deleted", Toast.LENGTH_SHORT).show();
						
						dialog.cancel();
					}
				});
				ad.show();
				return true;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
}
