package org.bitbucket.androidbacklog;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class NameAgencyActivity extends Activity {
	//private String nameAgency;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nameagency);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
}
