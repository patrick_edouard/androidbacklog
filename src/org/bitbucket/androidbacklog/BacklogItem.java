package org.bitbucket.androidbacklog;


import org.json.JSONException;
import org.json.JSONObject;

public class BacklogItem {
	public String commentary;
	public String estimation;
	public int id;
	public int name;
	public int priority;
	
	public BacklogItem(String commentary, String estimation, int i, int j, int k) {
		this.commentary = commentary;
		this.estimation = estimation;
		this.id = i;
		this.name = j;
		this.priority = k;
	}
	
	public static BacklogItem BacklogItemFromJSON(JSONObject o){
		try {
			return new BacklogItem(o.getString("commentary"), o.getString("estimation"), o.getInt("id"), o.getInt("name"), o.getInt("priority"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String toString() {
		return "BacklogItem [commentary=" + commentary + ", estimation="
				+ estimation + ", name=" + name + ", priority=" + priority
				+ "]";
	}
	
	public JSONObject toJSON() throws JSONException{
		JSONObject backLogItem = new JSONObject();
		backLogItem.put("commentary", this.commentary);
		backLogItem.put("estimation", this.estimation);
		backLogItem.put("id", this.id);
		backLogItem.put("name", this.name);
		backLogItem.put("priority", this.priority);
		return backLogItem;
	}
	
}
