package org.bitbucket.androidbacklog;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


public class MyProjectActivity extends Activity {

	NoSQLEngine myEngine = NoSQLEngine.getInstance();
	ListView listProject;

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		setContentView(R.layout.activity_myproject);


		/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

			// set title
			alertDialogBuilder.setTitle("Your Title");

			// set dialog message
			alertDialogBuilder
				.setMessage("Click yes to exit!")
				.setCancelable(false)
				.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						MainActivity.this.finish();
					}
				  })
				.setNegativeButton("No",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();
			}
		});*/

		//Si rien ne doit s'afficher
		listProject = (ListView) findViewById(R.id.listProject);
		List<String> listIfProjectEmpty = new ArrayList<String>();
		listIfProjectEmpty.add("");
		listIfProjectEmpty.add("No project yet");
		listProject.setEmptyView(null);
		//remplir la liste des projets en dur
		final List<String> projectList = new ArrayList<String>();
		
		projectList.addAll(myEngine.getUserProjects());
		
		ArrayAdapter<String> adapter;
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, projectList);
		listProject.setAdapter(adapter);
		if(listProject.getCount() == 0){
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listIfProjectEmpty);
			listProject.setAdapter(adapter);
		}

		final Button addButton = (Button) findViewById(R.id.addButton);
		addButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final AlertDialog.Builder ad = new AlertDialog.Builder(MyProjectActivity.this);
				final EditText input = new EditText(MyProjectActivity.this);
				ad.setTitle("New project");
				ad.setMessage("Please name this project");
				ad.setView(input);
				ad.setPositiveButton("Create",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						//MyProjectActivity.this.finish();
						String val = input.getText().toString().trim();
						projectList.add(val);
						ArrayAdapter<String> adapter;
						adapter = new ArrayAdapter<String>(MyProjectActivity.this, android.R.layout.simple_list_item_1, projectList);
						//adapter.remove((String) listProject.getItemAtPosition(adapter.getCount()));
						listProject.setAdapter(adapter);
						myEngine.createAgency(val);
						//String value = input.getText().toString().trim();
						Toast.makeText(getApplicationContext(), val+" has been created !", Toast.LENGTH_SHORT).show();
					}
				});
				ad.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});
				ad.show();
				//Intent intent = new Intent(MyProjectActivity.this, LoginActivity.class);
				//startActivity(intent);


			}
		});

		listProject.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parentOnClick, View viewOnClikc, int positionOnClick, long idOnClick)
			{
				if(!listProject.getItemAtPosition(positionOnClick).toString().equals("No project yet")){
					myEngine.setAgencyName(listProject.getItemAtPosition(positionOnClick).toString());
					Intent intent = new Intent(MyProjectActivity.this,MyTaskActivity.class);
					startActivity(intent);
				}
			}
		});

		listProject.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parentOnLongClick, View viewOnLongClick, final int positionOnLongClick, long idOnLongClick)
			{
				final AlertDialog.Builder ad = new AlertDialog.Builder(MyProjectActivity.this);
				final EditText input = new EditText(MyProjectActivity.this);
				ad.setTitle("Projet :"+listProject.getItemAtPosition(positionOnLongClick));
				ad.setMessage("Change name of your project");
				ad.setView(input);
				ad.setPositiveButton("Modify",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, close
						// current activity
						//MyProjectActivity.this.finish();
						//String value = input.getText().toString().trim();
						//Toast.makeText(getApplicationContext(), value+" has been created !", Toast.LENGTH_SHORT).show();

						//String value = (String) listProject.getItemAtPosition(positionOnLongClick);
						String val = (String) input.getText().toString().trim();
						listProject.getItemAtPosition(positionOnLongClick);

						//Toast.makeText(getApplicationContext(), positionOnLongClick + " has been modified", Toast.LENGTH_SHORT).show();
						projectList.add(val);
						ArrayAdapter<String> adapter;
						adapter = new ArrayAdapter<String>(MyProjectActivity.this, android.R.layout.simple_list_item_1, projectList);
						adapter.remove((String) listProject.getItemAtPosition(positionOnLongClick));

						//adapter.getItem(positionOnLongClick).replace((CharSequence) listProject.getItemAtPosition(positionOnLongClick), val);
						//temp = val;
						listProject.setAdapter(adapter);



						//startActivity(getIntent());
						Toast.makeText(getApplicationContext(), val + " has been modified", Toast.LENGTH_SHORT).show();
						dialog.cancel();


					}
				});
				ad.setNegativeButton("Delete",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						String value = listProject.getItemAtPosition(positionOnLongClick).toString();
						projectList.remove(listProject.getItemAtPosition(positionOnLongClick));
						ArrayAdapter<String> adapter;
						adapter = new ArrayAdapter<String>(MyProjectActivity.this, android.R.layout.simple_list_item_1, projectList);
						listProject.setAdapter(adapter);
						Toast.makeText(getApplicationContext(), value + " has been deleted", Toast.LENGTH_SHORT).show();
						dialog.cancel();
					}
				});
				ad.show();
				//Intent intent = new Intent(MyProjectActivity.this, LoginActivity.class);
				//startActivity(intent);
				return true;
			}
		});

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
}
