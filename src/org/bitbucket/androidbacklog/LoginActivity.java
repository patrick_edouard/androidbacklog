package org.bitbucket.androidbacklog;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity {

	private NoSQLEngine myEngine;
	private String login;
	private String password;
	private EditText txtLogin;
	private EditText txtPassword;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		setContentView(R.layout.activity_login);

		myEngine = NoSQLEngine.getInstance();
		txtLogin = (EditText) findViewById(R.id.login);
		txtPassword = (EditText) findViewById(R.id.password);

		final Button signinButton = (Button) findViewById(R.id.signinLogin);
		signinButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LoginActivity.this, SigninActivity.class);
				startActivity(intent);
			}
		});

		final Button loginButton = (Button) findViewById(R.id.loginLogin);
		loginButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				login = txtLogin.getText().toString();
				password = txtPassword.getText().toString();

				Log.i(LoginActivity.class.getName(),"Login :"+login+"Password :"+password);

				if(myEngine.connectUser(login, password)) {
					Toast.makeText(LoginActivity.this, "Connection success", Toast.LENGTH_LONG).show();
					Intent intent = new Intent(LoginActivity.this, MyProjectActivity.class);
					startActivity(intent);
				} else
					Toast.makeText(LoginActivity.this, "Connection fail", Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
}
