package org.bitbucket.androidbacklog;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class NoSQLEngine {
	
	private static NoSQLEngine instance;
	//private static String dbUrl = "http://192.168.139.180:8080/ScrumServer/api/";
	//private static String dbUrl = "http://mmi-interne.univ-savoie.fr/thibault.carron/ScrumServer/";
	//private static String dbUrl = "193.48.121.162/thibault.carron/ScrumServer/";
	private static String dbUrl = "http://193.48.120.104:8080/";
	private HttpClient httpclient;
	private String user = "";
	@SuppressWarnings("unused")
	private boolean isConnected = false;
	private String agencyName = "ACME";
	
	//infos concerning the user's agile dash board
	private ArrayList<BacklogItem> tasks;	
	
	private NoSQLEngine(){
		//this.tasks=new ArrayList<BacklogItem>();
		this.connexion();
		this.loadBackLogItem();
	}
	
	/**
	 * GetInstance
	 * 
	 * Return the only instance of NoSQLEngine
	 * (Pattern singleton)
	 * 
	 * @return instance The unique instance
	 */
	public static NoSQLEngine getInstance(){
		
		if(NoSQLEngine.instance==null){
			NoSQLEngine.instance=new NoSQLEngine();
		}
		return NoSQLEngine.instance;
	}
	
	public ArrayList<BacklogItem> getBacklogItems(){
		return this.tasks;
	}
	
	private void connexion(){
		this.httpclient = new DefaultHttpClient(); // for port 80 requests!
	}
	
	
	public void sendBacklogItem(BacklogItem item){
		try {
			String url = NoSQLEngine.dbUrl+"ScrumServer/api/v0.1/agency/"+agencyName+"/line";
			System.err.println(url);
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			
			JSONObject o = new JSONObject();
			o = item.toJSON();
			System.err.println(o);
			System.err.println(item.toString());
			StringEntity se = new StringEntity(o.toString());
			httpPost.setEntity(se);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");
		    ResponseHandler responseHandler = new BasicResponseHandler();
		    httpclient.execute(httpPost, responseHandler);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteTask(int taskID){
		try {
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpDelete httpDelete = new HttpDelete(NoSQLEngine.dbUrl+"/ScrumServer/api/v0.1/agency/"+agencyName+"/line/"+taskID);
			System.err.println(NoSQLEngine.dbUrl+"ScrumServer/api/v0.1/agency/"+agencyName+"/line/"+taskID);
			
			//httpDelete.setHeader("Accept", "application/json");
		    //httpDelete.setHeader("Content-type", "application/json");
		    ResponseHandler responseHandler = new BasicResponseHandler();
		    httpclient.execute(httpDelete, responseHandler);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteAgency(String agencyName){
		try {
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpDelete httpDelete = new HttpDelete(NoSQLEngine.dbUrl+"/ScrumServer/api/v0.1/agency/"+agencyName);
			
			//httpDelete.setHeader("Accept", "application/json");
		    //httpDelete.setHeader("Content-type", "application/json");
		    ResponseHandler responseHandler = new BasicResponseHandler();
		    httpclient.execute(httpDelete, responseHandler);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void createAgency(String agencyName){
		try {
			String url = NoSQLEngine.dbUrl+"/ScrumServer/api/v0.1/agency";
			
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httPost = new HttpPost(url);
			JSONObject agency = new JSONObject();
			agency.put("name", agencyName);
			StringEntity se = new StringEntity(agency.toString());
			//System.err.println(agency.toString());
			
			httPost.setEntity(se);
		    httPost.setHeader("Content-type", "application/json");
		    ResponseHandler responseHandler = new BasicResponseHandler();
		    String jsonResponse = httpclient.execute(httPost, responseHandler);
		    //System.out.println(jsonResponse);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void loadBackLogItem(){
		HttpGet httpGet = new HttpGet(NoSQLEngine.dbUrl+"/ScrumServer/api/v0.1/agency/"+agencyName);
		String ligne = "";
		String backlog= "";
		this.tasks=new ArrayList<BacklogItem>();
		
		try {
			
			HttpResponse response = httpclient.execute(httpGet);
			StatusLine statusline = response.getStatusLine();
			HttpEntity entity = response.getEntity();
			InputStream is = entity.getContent();
			BufferedReader bufferedreader=new BufferedReader( new InputStreamReader(is));
			
			ligne=bufferedreader.readLine();
			while (ligne!=null){
				backlog+=ligne;
				ligne=bufferedreader.readLine(); 
			}
			
		} catch (ClientProtocolException e) {
			//serveur innexistant, on met la liste des taches à null
			this.tasks = new ArrayList<BacklogItem>();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			JSONObject jsonBacklog = new JSONObject(backlog);
			JSONArray lol = jsonBacklog.getJSONArray("backlog");
			for(int i = 0; !lol.isNull(i); ++i){
				JSONObject tache = lol.getJSONObject(i);
				this.tasks.add(BacklogItem.BacklogItemFromJSON(tache));
				//System.out.println(lol.getJSONObject(i));
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void modifyTask(BacklogItem item){
		try {
			String url = NoSQLEngine.dbUrl+"/ScrumServer/api/v0.1/line";
			
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPut httPost = new HttpPut(url);
			JSONObject agency = new JSONObject();
			JSONObject o = new JSONObject();
			o = item.toJSON();
			System.err.println(o);
			System.err.println(item.toString());
			StringEntity se = new StringEntity(o.toString());
			//System.err.println(agency.toString());
			
			httPost.setEntity(se);
		    httPost.setHeader("Content-type", "application/json");
		    ResponseHandler responseHandler = new BasicResponseHandler();
		    String jsonResponse = httpclient.execute(httPost, responseHandler);
		    //System.out.println(jsonResponse);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public String getQuelqueChose(){
		HttpGet HttpGet = new HttpGet(NoSQLEngine.dbUrl+"truc");
		HttpResponse response;
		String ligne = "";
		try {
			response = httpclient.execute(HttpGet);
			HttpEntity entity = response.getEntity();
			InputStream is = entity.getContent();

			BufferedReader bufferedreader=new BufferedReader( new InputStreamReader(is));
			
			ligne=bufferedreader.readLine();
			while (ligne!=null){ 
				ligne=bufferedreader.readLine(); 

			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ligne;
	}
	
	public boolean connectUser(String user, String pass){
		if(user.equals("nchal")){
			if(pass.equals("123")){
				this.user = user;
				this.isConnected = true;
				return true;
			}
		}else if(user.equals("cylai")){
			if(pass.equals("dellounet")){
				this.user = user;
				this.isConnected = true;
				return true;
			}
		}else if(user.equals("proux")){
			if(pass.equals("carve")){
				this.user = user;
				this.isConnected = true;
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * @return the agencyName
	 */
	public String getAgencyName() {
		return agencyName;
	}

	/**
	 * @param agencyName the agencyName to set
	 */
	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
		this.loadBackLogItem();
	}
	
	public ArrayList<String> getUserProjects(){
		ArrayList<String> projets = new ArrayList<String>();
		if(this.user.equals("nchal")){
			projets.add("ACME");
			projets.add("CarvingInc");
		}else if(this.user.equals("cylai")){
			projets.add("Cyril faire un projet ? lol.");
		}else if(this.user.equals("proux")){
			projets.add("ACME");
			projets.add("CarvingInc");
		}
		return projets;
	}
	
	
	/**
	 * test local
	 */
	public static void main(String[] args){
		NoSQLEngine db = NoSQLEngine.getInstance();
		
		/*
		for(BacklogItem item : db.getBacklogItems()){
			System.out.println(item);
		}*/
		
		
		//db.sendBacklogItem(new BacklogItem("Premiere tache à effectuer", "10j", 3, 5, 10));
		
		/**
		 * Delete 100 tache en bourrinant
		 
		
		for(int i = 0; i<=100; ++i){
			db.deleteTask(i);
		}
		*/
	}

	
}
